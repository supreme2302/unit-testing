const server = require('../server.js');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const chai = require('chai');
let chaiHttp = require('chai-http');


let should = chai.should();

chai.use(chaiHttp);
const { COPYFILE_EXCL } = fs.constants;

let indexPath = null;

describe('GET request', () => {
	let app;
	before((done) => {
		app = server.listen(3333, () => {
			done();

		});
	});

	it('it should GET /index.html', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
                assert.equal(res.text, fs.readFileSync(config.publicRoot + '/index.html', 'utf-8'));
              done();
            });
      });

	it('it should GET /file.txt', done => {
		chai.request(server)
		.get('/file.txt')
		.end((err, res) => {
			assert.equal(res.text, fs.readFileSync(config.filesRoot + '/file.txt', 'utf-8'));
			done();
		});
	});
	

	// Написать тесты на
	// get запрос к http://localhost:3333  => вернет index.html
	// get запрос к http://localhost:3333/file.txt => вернет file.txt
	after((done) => {
		app.close(() => {
		done();
		});
	});

});
